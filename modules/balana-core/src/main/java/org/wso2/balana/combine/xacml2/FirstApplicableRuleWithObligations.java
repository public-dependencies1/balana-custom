package org.wso2.balana.combine.xacml2;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.wso2.balana.ObligationResult;
import org.wso2.balana.Rule;
import org.wso2.balana.combine.RuleCombinerElement;
import org.wso2.balana.combine.RuleCombiningAlgorithm;
import org.wso2.balana.ctx.AbstractResult;
import org.wso2.balana.ctx.EvaluationCtx;
import org.wso2.balana.ctx.ResultFactory;
import org.wso2.balana.ctx.xacml3.Result;

public class FirstApplicableRuleWithObligations extends RuleCombiningAlgorithm {

	/**
	 * The standard URN used to identify this algorithm
	 */
	public static final String algId = "urn:oasis:names:tc:xacml:1.0:rule-combining-algorithm:"
			+ "first-applicable-with-obligations";

	// a URI form of the identifier
	private static URI identifierURI;
	// exception if the URI was invalid, which should never be a problem
	private static RuntimeException earlyException;

	static {
		try {
			identifierURI = new URI(algId);
		} catch (URISyntaxException se) {
			earlyException = new IllegalArgumentException();
			earlyException.initCause(se);
		}
	}

	/**
	 * Standard constructor.
	 */
	public FirstApplicableRuleWithObligations() {
		super(identifierURI);

		if (earlyException != null)
			throw earlyException;
	}

	/**
	 * Protected constructor used by the ordered version of this algorithm.
	 * 
	 * @param identifier the algorithm's identifier
	 */
	protected FirstApplicableRuleWithObligations(URI identifier) {
		super(identifier);
	}

	/**
	 * Applies the combining rule to the set of rules based on the evaluation
	 * context.
	 * 
	 * @param context      the context from the request
	 * @param parameters   a (possibly empty) non-null <code>List</code> of
	 *                     <code>CombinerParameter<code>s
	 * @param ruleElements the rules to combine
	 * 
	 * @return the result of running the combining algorithm
	 */
	@Override
	public AbstractResult combine(EvaluationCtx context, List parameters, List ruleElements) {
		AbstractResult result = ResultFactory.getFactory().getResult(Result.DECISION_NOT_APPLICABLE, context);
		int decision = Result.DECISION_INDETERMINATE;
		Iterator it = ruleElements.iterator();
		while (it.hasNext()) {
			Rule rule = ((RuleCombinerElement) (it.next())).getRule();
			AbstractResult currentResult = rule.evaluate(context);
			if (currentResult.getDecision() != Result.DECISION_NOT_APPLICABLE
					&& decision == Result.DECISION_INDETERMINATE) {
				result = currentResult;
				decision = result.getDecision();
				addToResult(result, currentResult.getObligations());
			} else if (currentResult.getDecision() == decision) {
				addToResult(result, currentResult.getObligations());
			}
		}
		return result;
	}

	private void addToResult(AbstractResult result, List<ObligationResult> obligationResultList) {
		if (result.getDecision() != Result.DECISION_NOT_APPLICABLE) {	
			for (ObligationResult obligationResult : obligationResultList) {
				if (!result.getObligations().contains(obligationResult)) {
					result.addObligations(obligationResult);
				}
			}
		}
	}
}
